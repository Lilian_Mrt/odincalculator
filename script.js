//GRAPHICAL PART
const container = document.querySelector(".container");
const display = document.querySelector(".display");
const buttons = document.querySelectorAll("button");
const digits = document.querySelectorAll(".digit");
const ops = document.querySelectorAll(".op");
const equal = document.querySelector("#eq");
equal.disabled = true;
const ac = document.querySelector("#ac");
const coma = document.querySelector("#coma");
const del = document.querySelector("#del");


function resizeGrid() {
  display.removeAttribute("style");
  buttons.forEach((button) => {
    button.removeAttribute("style");
  });
  const width = window.innerWidth;
  const height = window.innerHeight;
  let dim;
  width < height
    ? (dim = width - 20)
    : (dim = height - 20);
  container.style.width = `${dim}px`;
  container.style.height = `${dim}px`;
}

function fontSizeUpdate() {
  resizeGrid();
  const heightDisplay = display.offsetHeight;
  display.setAttribute("style", `font-size : ${heightDisplay}px`);
  buttons.forEach((button) => {
    button.setAttribute("style", `font-size : ${heightDisplay}px`);
  });
}

fontSizeUpdate();

window.addEventListener("resize", () => fontSizeUpdate());



//CALCULATOR PART
function add(n1, n2) {
  return n1 + n2;
}

function subtract(n1, n2) {
  return n1 - n2;
}

function multiply(n1, n2) {
  return n1 * n2;
}

function divide(n1, n2) {
  return n1 / n2;
}

function operate(n1, n2, op) {
  switch (op) {
    case 0:
      return add(n1, n2);
    case 1:
      return subtract(n1, n2);
    case 2:
      return multiply(n1, n2);
    case 3:
      return divide(n1, n2);
    default:
      console.log("Error not known operator");
  }
}

let displayValue = "";
let number1 = 0;
let number2;
let operator;
let numberStored = false;
let resultDisplayed = false;
let operatorSelectionned =false;

digits.forEach((digit) => {
  digit.addEventListener("click",() => {
    if (numberStored){
      displayValue = ""
      numberStored = false;
    }
    if (digit.getAttribute('id') == "coma"){
      if (displayValue ==""){
        displayValue ="0.";
      } else {
        displayValue += digit.textContent;
      }
      digit.disabled = true;
    } else if (displayValue != "0") {
      displayValue += digit.textContent;
    } else {
      displayValue = digit.textContent;
    }
    resultDisplayed = false;
    operatorSelectionned = false;
    ops.forEach((ope) => {
      ope.classList.remove("operatorSelected");
    });
    display.textContent = displayValue;
  })
});

ops.forEach((op) =>{
  const ope = op.textContent;
  let justDisplayed = false;
  op.addEventListener("click",() =>{
    if (!equal.disabled && !operatorSelectionned) {
      number2 = parseFloat(displayValue);
      const result = operate(number1, number2, operator);
      result == Infinity ? displayValue = "ERROR" : displayValue = (Math.round(result * 10) / 10).toString();
      display.textContent = displayValue;
      resultDisplayed = true;
      justDisplayed = true;
      if (displayValue == "ERROR"){
        buttons.forEach((button) => {
          if (button != ac){
            button.disabled = true;
          }
        })
        return;};
      equal.disabled = true;
      numberStored = true;
    }
    if (ope == "+") {
      operator = 0;
    } else if (ope == "-") {
      operator = 1;
    } else if (ope == "*") {
      operator = 2;
    } else {
      operator = 3;
    }
    if (displayValue != ""){
      number1 = parseFloat(displayValue);
    }
    if (!justDisplayed){
      displayValue = "";
    }
    operatorSelectionned = true;
    ops.forEach((ope) => {
      ope.classList.remove("operatorSelected");
    })
    op.classList.add("operatorSelected");
    equal.disabled = false;
    coma.disabled = false;
  })
});

equal.addEventListener("click",() =>{
  number2 = parseFloat(displayValue);
  const result = operate(number1,number2,operator);
  result == Infinity ? displayValue = "ERROR" : displayValue = (Math.round(result * 10) / 10).toString();
  display.textContent = displayValue;
  if (displayValue == "ERROR"){
        buttons.forEach((button) => {
          if (button != ac){
            button.disabled = true;
          }
        })
        return;};
  equal.disabled =true;
  numberStored =true;
  coma.disabled = false;
  operatorSelectionned = false;
  ops.forEach((ope) => {
    ope.classList.remove("operatorSelected");
  });
  resultDisplayed =true;
});


ac.addEventListener("click",()=>{
  displayValue = "";
  number1 = 0;
  numberStored = false;
  resultDisplayed = false;
  operatorSelectionned = false;
  ops.forEach((ope) => {
    ope.classList.remove("operatorSelected");
  });
  buttons.forEach((button) => {
    if (button == equal) {
      button.disabled = true;
    } else {
      button.disabled = false;
    }
  });
  display.textContent = "0";
})

del.addEventListener("click",() =>{
  if (resultDisplayed){
  } else if (displayValue == "0." || displayValue == ""){
    displayValue = "";
    display.textContent = "0";
    coma.disabled = false;
  } else{
    if (displayValue.charAt(displayValue.length - 1) == ".") {
      coma.disabled = false;
    }
    displayValue = displayValue.substring(0, displayValue.length - 1);
    if (displayValue == "")
      display.textContent = "0";
    else {
      display.textContent = displayValue;
    }
  }
});


window.addEventListener("keydown",function(event){
  switch (event.key) {
    case "0":
      document.getElementById("zero").click();
      break;
    case "1":
      document.getElementById("one").click();
      break;
    case "2":
      document.getElementById("two").click();
      break;
    case "3":
      document.getElementById("three").click();
      break;
    case "4":
      document.getElementById("four").click();
      break;
    case "5":
      document.getElementById("five").click();
      break;
    case "6":
      document.getElementById("six").click();
      break;
    case "7":
      document.getElementById("seven").click();
      break;
    case "8":
      document.getElementById("eight").click();
      break;
    case "9":
      document.getElementById("nine").click();
      break;
    case "+":
      document.getElementById("plus").click();
      break;
    case "-":
      document.getElementById("minus").click();
      break;
    case "*":
      document.getElementById("times").click();
      break;
    case "/":
      document.getElementById("by").click();
      break;
    case ".":
      document.getElementById("coma").click();
      break;
    case "Enter":
      document.getElementById("eq").click();
      break;
    case "Backspace":
      document.getElementById("del").click();
      break;
    case "Shift":
      document.getElementById("ac").click();
      break;

  }
});

buttons.forEach((button) => {
  button.addEventListener("click",()=> {
    button.classList.add("playing");
  })
  button.addEventListener("transitionend", (e) => {
    if (e.propertyName == "transform"){
      button.classList.remove("playing");
    }
  });
})

